class_name Order

enum NAMES {
	HOLD, 
	SUPPORT,
	MOVE,
	CONVOY
}

var action
var from
var to
var what
var verb setget ,get_verb

func get_verb():
	match action:
		NAMES.CONVOY: return 'convoys'
		NAMES.HOLD: return 'holds'
		NAMES.MOVE: return 'moves'
		NAMES.SUPPORT: return 'supports'
