tool
extends Node

class_name Nations

enum NAMES {
	NEUTRAL, FRANCE, ENGLAND, ITALY, GERMANY, AUSTRIA, RUSSIA, TURKEY
}

const COLORS = {
	NAMES.FRANCE : "#0058be",
	NAMES.ENGLAND : "#FC8A25",
	NAMES.GERMANY : "#D6B04F",
	NAMES.AUSTRIA : "#FC8889",
	NAMES.RUSSIA : "#9158D8",
	NAMES.ITALY : "#6CCD67",
	NAMES.TURKEY: "#46C5FC"
}

static func get_color(country):
	if country == NAMES.NEUTRAL:
		return Color(1,1,1,0)
	else :
		return Color(COLORS[country])
