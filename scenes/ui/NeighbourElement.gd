extends HBoxContainer

signal delete_request(neighbour_element, slot)

var slot

func _ready():
	var type = "Fleet" if slot.is_fleet else "Army"
	$Label.text = "%s of %s" % [type, slot.get_territory().territory_name]

func _on_Delete_pressed():
	emit_signal("delete_request", self, slot)
