extends Node

var label_order_packed = preload("res://scenes/ui/OrderLabel.tscn")
	
func get_map() :
	return $MainContainer/HBoxContainer/ViewportContainer/Viewport/GameplayMap

func display_actions(display: bool):
	$MainContainer/HBoxContainer/PanelContainer/Panel/VBoxContainer/Actions.visible = display

func get_orders() :
	return $MainContainer/HBoxContainer/PanelContainer/Panel/VBoxContainer/Orders

func get_label_msg():
	return $MainContainer/HBoxContainer/PanelContainer/Panel/VBoxContainer/LabelActions

# Called when the node enters the scene tree for the first time.
func _ready():
	pass

func _on_GameplayMap_troup_deselected(troup : Troup):
	display_actions(false)
	get_label_msg().text = "Select a troup"

func _on_GameplayMap_troup_selected(troup: Troup):
	get_label_msg().text = "What should %s in %s do ?" % [troup.get_name(), troup.territory.get_name()]
	display_actions(true)
	var convoy_btn = $MainContainer/HBoxContainer/PanelContainer/Panel/VBoxContainer/Actions/ButtonConvoy
	convoy_btn.visible = troup.is_fleet

func _on_ButtonMove_pressed():
	display_actions(false)
	get_map().set_state(GameplayMap.STATES.MOVE)
	var selected_troup = get_map().selected_troup
	get_label_msg().text = "Where should %s move to ?" % [selected_troup.name]

func _on_ButtonHold_pressed():
	display_actions(false)
	get_map().set_state(GameplayMap.STATES.HOLD)

func _on_ButtonSupport_pressed():
	display_actions(false)
	get_map().set_state(GameplayMap.STATES.SUPPORT_FROM)
	var selected_troup = get_map().selected_troup
	get_label_msg().text = "What troup should %s support ?" % [selected_troup.name]

func _on_ButtonConvoy_pressed():
	display_actions(false)
	get_map().set_state(GameplayMap.STATES.CONVOY_FROM)
	var selected_troup = get_map().selected_troup
	get_label_msg().text = "What troup should %s convoy ?" % [selected_troup.name]
	
func _on_GameplayMap_order_sent(order):
	var order_label = label_order_packed.instance()
	order_label.text = ""
	if order.action in [Order.NAMES.CONVOY, Order.NAMES.SUPPORT]:
		order_label.text = "%s %s %s to %s" % [order.from, order.verb, order.what, order.to]
	elif order.action == Order.NAMES.MOVE:
		order_label.text = "%s %s to %s" % [order.from, order.verb, order.to]
	else : 
		order_label.text = "%s %s" % [order.from, order.verb]
		
	get_orders().add_child(order_label)


func _on_GameplayMap_troup_convoyed(troup):
	get_label_msg().text = "Where should %s \n in %s be convoyed to ?" % [troup.get_name(), troup.territory.get_name()]


func _on_GameplayMap_troup_supported(troup):
	get_label_msg().text = "Where should %s \n in %s be supported to ?" % [troup.get_name(), troup.territory.get_name()]

