extends Camera2D

export(float, 0.5, 2, 0.1) var zoom_min = 0.5;
var zoom_scalar: float = 2;
var dragging: = false;
var zoom_max

# Called when the node enters the scene tree for the first time.
func _ready():
	setup_zoom()
	
func setup_zoom():
	var background = $'../BackgroundColor'
	var map_size = background.get_size() * background.rect_scale
	var viewport_size = get_viewport().size
	zoom_max = min(map_size.x / viewport_size.x, map_size.y / viewport_size.y)
	
	set_zoom_min()
	zoom_scalar = zoom_max
	clamp_zoom()
	position = (map_size / 2)
	limit_left = background.rect_position.x
	limit_right = background.rect_position.x + map_size.x
	limit_top = background.rect_position.y
	limit_bottom = background.rect_position.y + map_size.y
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	dragging = Input.is_action_pressed("drag")

func _unhandled_input(event):
	if event.is_action("zoom_out"):
		zoom_scalar += 0.2
		clamp_zoom()
	elif event.is_action("zoom_in"):
		zoom_scalar -= 0.2
		clamp_zoom()
	elif event is InputEventMouseMotion and dragging:
		position -= event.relative * 2.0
		var cam_size: = get_viewport().size / zoom
		clamp_cam_pos()
	
func clamp_cam_pos(): 
	var cam_pos: Vector2 = position
	var view_size: = get_viewport_rect().size * zoom * (1 - drag_margin_left) / 2
	
	cam_pos.x = clamp(cam_pos.x, limit_left + view_size.x, limit_right - view_size.x)
	cam_pos.y = clamp(cam_pos.y, limit_top + view_size.y, limit_bottom - view_size.y)
	position = cam_pos

func set_zoom_min(): 
	if zoom_min > zoom_max : zoom_min = zoom_max
	clamp_zoom()
	
func clamp_zoom():
	zoom_scalar = clamp(zoom_scalar, zoom_min, zoom_max)
	zoom = Vector2(zoom_scalar, zoom_scalar)
	clamp_cam_pos()
