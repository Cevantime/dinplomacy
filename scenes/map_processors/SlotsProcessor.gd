extends "res://scenes/MapProcessor.gd"

var slot_selected
var slot_entered
var slot_inspected
var slot_dragged
var old_slot_dragged_territory
var new_slot

var slot_packed = preload("res://scenes/Slot.tscn")
	
func _start():
	_disable()
	
func connect_everything():
	for territory in get_tree().get_nodes_in_group("Territory"):
		self.connect_territory(territory)
	for slot in get_tree().get_nodes_in_group("Slot"):
		self.connect_slot(slot)
		
func disconnect_everything():
	for territory in get_tree().get_nodes_in_group("Territory"):
		self.disconnect_territory(territory)
	for slot in get_tree().get_nodes_in_group("Slot"):
		self.disconnect_slot(slot)
		
func _process(delta):
	if Input.is_action_just_pressed("touched"):
		if !new_slot and slot_entered == null and slot_selected:
			unselect_slot()
			map.get_node("PathsRenderer").current_target = null
		elif new_slot:
			undrag_slot()
			new_slot = null
			yield(get_tree(), "idle_frame")
			map.get_node("SlotsRenderer").update()
			
	if Input.is_action_just_released("touched"):
		if slot_dragged and !new_slot:
			undrag_slot()

func undrag_slot():
	var territory_to_attach = old_slot_dragged_territory
			
	if map.get_territory_by_state("Dragged") != null:
		territory_to_attach = map.get_territory_by_state("Dragged")
	
	if territory_to_attach != null :
		map.get_main_area().remove_child(slot_dragged)
		territory_to_attach.add_slot(slot_dragged)
		slot_dragged.position = slot_dragged.position - territory_to_attach.global_position
		slot_dragged.dragged= false
		map.unhighlight_territory(territory_to_attach, "Dragged")
	else :
		slot_dragged.queue_free()
	
	slot_dragged = null
		
	map.emit_signal("slot_drag_end", slot_dragged)
	
func _on_slot_is_fleet_changed(slot, is_fleet):
	map.get_node("SlotsRenderer").reset_slot_color(slot)
	
func _on_slot_clicked(slot):
	if new_slot:
		return
	var tmp = slot_inspected
	slot_inspected = null
	map.emit_signal("slot_uninspected", tmp)
	if slot_selected == null:
		slot_selected = slot
		# map.get_node("SlotsRenderer").change_slot_color(slot, Color.red)
		map.get_node("PathsRenderer").slot = slot
	elif slot != slot_selected : 
		slot_selected.add_neighbour(slot)
	else :
		unselect_slot()
	
func delete_slot(slot):
	slot.remove_neighbours()
	slot.queue_free()
	yield(get_tree(), "idle_frame")
	map.get_node("SlotsRenderer").update()
	
func create_slot():
	var slot = slot_packed.instance()
	connect_slot(slot)
	make_slot_dragged(slot)
	new_slot = slot
	
func _on_slot_inspected(slot):
	slot_inspected = slot
	map.emit_signal("slot_inspected", slot)
	
func unselect_slot():
	slot_selected = null
	map.get_node("PathsRenderer").slot = null

func _on_slot_entered(slot):
	if new_slot :
		return
	slot_entered = slot
	if slot_selected == null :
		map.get_node("PathsRenderer").slot = slot
	
func _on_slot_exited(slot):
	if new_slot :
		return
	if map.get_node("PathsRenderer").slot == slot_entered and slot_entered != slot_selected:
		map.get_node("PathsRenderer").slot = null
	slot_entered = null

func _on_slot_drag_start(slot):
	# detach slot to be able to drag it
	var territory = slot.get_territory()
	territory.remove_slot(slot)
	old_slot_dragged_territory = territory
	map.highlight_territory(territory, "Dragged")
	make_slot_dragged(slot)
	
func make_slot_dragged(slot):
	var new_parent = map.get_main_area()
	new_parent.add_child(slot)
	slot.set_owner(new_parent)
	slot.position = map.get_global_mouse_position()
	slot_dragged = slot
	map.emit_signal("slot_drag_start", slot)
	
func _on_territory_entered(territory):
	if slot_dragged :
		print("highlight")
		map.highlight_territory(territory, "Dragged")
		
func _on_territory_exited(territory):
	if slot_dragged:
		map.unhighlight_territory(territory, "Dragged") 
		print("unhighlight ", map.get_territory_by_state("Dragged"))
	
func _unhandled_input(event):
	if slot_selected:
		map.get_node("PathsRenderer").current_target = map.get_global_mouse_position()
	elif slot_dragged:
		slot_dragged.position = map.get_global_mouse_position()
		
func _import(data):
	if not data.has("slots") or not data.has("territories"):
		return
	
	data._slots_by_id = {} if not data.has("_slots_by_id") else data._slots_by_id
	for territory_id in data.territories:
		var territory_data = data.territories[territory_id]
		var territory = data._territories_by_id[territory_id]
		if is_processing() : connect_territory(territory)
		for slot_id in territory_data.slots:
			if not data.slots.has(str(slot_id)):
				# TODO generate a warning or something
				print("No slot conf for slot " + slot_id)
				continue
			var slot_data = data.slots[str(slot_id)]
			var slot = slot_packed.instance()
			slot.is_fleet = "fleet" == slot_data.type
			slot.position = Vector2(slot_data.position[0], slot_data.position[1])
			slot.slot_name = slot_data.name
			territory.get_node("Slots").add_child(slot)
			data._slots_by_id[slot_id] = slot
			if is_processing() : connect_slot(slot)
	

	for slot_id in data._slots_by_id:
		var slot = data._slots_by_id[slot_id]
		var slot_data = data.slots[str(slot_id)]
		for neighbour_id in slot_data.neighbours:
			slot.add_neighbour(data._slots_by_id[neighbour_id])
	
	yield(get_tree(), "idle_frame")
	
	get_map().get_node("SlotsRenderer").update()

func _save(data):
	data.slots = {} if not data.has('slots') else data.slots
	for slot in get_tree().get_nodes_in_group("Slot"):
		var slot_data = {} if not data.slots.has(slot.get_instance_id()) else data.slots[slot.get_instance_id()]
		
		slot_data.neighbours = []
		
		for neighbour in slot.neighbours :
			if slot_data.neighbours.find(neighbour.get_instance_id()) == -1:
				slot_data.neighbours.push_back(neighbour.get_instance_id())
			
		slot_data.name = slot.slot_name
		slot_data.type = "fleet" if slot.is_fleet else "army"
		
		slot_data.position = [ slot.position.x, slot.position.y ]
		
		data.slots[slot.get_instance_id()] = slot_data
	
	data.territories = {} if not data.has('territories') else data.territories
	
	for territory in get_tree().get_nodes_in_group("Territory"):
		var territory_data = {} if not data.territories.has(territory.get_instance_id()) else data.territories[territory.get_instance_id()]
		territory_data.slots = []
		
		for slot in territory.get_node("Slots").get_children():
			territory_data.slots.push_back(slot.get_instance_id())
		
		data.territories[territory.get_instance_id()] = territory_data
	
	return data
		
func connect_territory(territory):
	territory.connect("entered", self, "_on_territory_entered")
	territory.connect("exited", self, "_on_territory_exited")
	
func disconnect_territory(territory):
	territory.disconnect("entered", self, "_on_territory_entered")
	territory.disconnect("exited", self, "_on_territory_exited")
	
func connect_slot(slot):
	slot.connect("clicked", self, "_on_slot_clicked")
	slot.connect("inspected", self, "_on_slot_inspected")
	slot.connect("entered", self, "_on_slot_entered")
	slot.connect("exited", self, "_on_slot_exited")
	slot.connect("is_fleet_changed", self, "_on_slot_is_fleet_changed")
	slot.connect("drag_start", self, "_on_slot_drag_start")
	slot.connect("is_fleet_changed", map.get_node("SlotsRenderer"), "_on_is_fleet_changed")
	slot.connect("transform_changed", map.get_node("SlotsRenderer"), "_on_transform_changed")
	slot.connect("transform_changed", map.get_node("PathsRenderer"), "refresh")
	map.get_node("SlotsRenderer").reset_slot_color(slot)

func disconnect_slot(slot):
	slot.disconnect("clicked", self, "_on_slot_clicked")
	slot.disconnect("inspected", self, "_on_slot_inspected")
	slot.disconnect("entered", self, "_on_slot_entered")
	slot.disconnect("exited", self, "_on_slot_exited")
	slot.disconnect("is_fleet_changed", self, "_on_slot_is_fleet_changed")
	slot.disconnect("drag_start", self, "_on_slot_drag_start")
	slot.disconnect("is_fleet_changed", map.get_node("SlotsRenderer"), "_on_is_fleet_changed")
	slot.disconnect("transform_changed", map.get_node("SlotsRenderer"), "_on_transform_changed")
	slot.disconnect("transform_changed", map.get_node("PathsRenderer"), "refresh")
	map.get_node("SlotsRenderer").reset_slot_color(slot)
	
func _disable():
	get_map().get_node("PathsRenderer").visible = false
	get_map().get_node("SlotsRenderer").opacity = 0.3
	get_map().get_node("SlotsRenderer").update()
	disconnect_everything()
	
func _enable():
	get_map().get_node("PathsRenderer").visible = true
	get_map().get_node("SlotsRenderer").opacity = 1
	get_map().get_node("SlotsRenderer").update()
	connect_everything()
