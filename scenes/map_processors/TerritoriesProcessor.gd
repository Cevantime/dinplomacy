extends "res://scenes/MapProcessor.gd"

var territory_packed = preload("res://scenes/Territory.tscn")
var polygon_point_packed = preload("res://scenes/editor/PolygonPoint.tscn")

var territory_edited
var polygon_point_selected
var new_polygon_point: PolygonPoint
var added_point_informations
var polygon_point_inspected
var polygon_entered_stack = []

func _start():
	print("territories started")
	

func _process(delta):
	if Input.is_action_just_pressed("touched") :
		if new_polygon_point:
			# check if polygon has to be closed
			var formed_polygon = get_map().get_node("PolygonPointRenderer").start_point.get_polygon(true)
			formed_polygon.pop_back()
			var closing_point = null
			for point in formed_polygon :
				if new_polygon_point.position.distance_to(point.position) < 5.0 :
					closing_point = point
					break
			if closing_point:
				closing_point.previous = new_polygon_point
				new_polygon_point.next = closing_point
				var territory_polygon = closing_point.get_polygon()
				territory_polygon.pop_back()
				var new_territory = territory_packed.instance()
				new_territory.get_node("CollisionPolygon2D").polygon = territory_polygon
				var p = new_territory.get_node("CollisionPolygon2D").polygon
				get_map().get_main_area().add_child(new_territory)
				connect_territory(new_territory)
				get_map().get_node("NewPolygonRenderer").start_point = null
				get_map().get_node("PolygonPointRenderer").start_point = null
				select_territory(new_territory)
				polygon_entered_stack = []
				new_polygon_point = null
			else :
				var new_point = polygon_point_packed.instance()
				new_point.position = new_polygon_point.position
				connect_polygon_point(new_point)
				get_map().get_main_area().add_child(new_point)
				var old_point = new_polygon_point
				new_polygon_point = new_point
				new_polygon_point.previous = old_point
				old_point.next = new_polygon_point
			
			refresh_polygon_points()
			
		elif added_point_informations:
			polygon_point_selected = polygon_point_packed.instance()
			polygon_point_selected.position = get_map().get_global_mouse_position()
			connect_polygon_point(polygon_point_selected)
			get_map().get_main_area().add_child(polygon_point_selected)
			polygon_point_selected.previous = added_point_informations.previous
			polygon_point_selected.previous.next = polygon_point_selected
			polygon_point_selected.next = added_point_informations.next
			polygon_point_selected.next.previous = polygon_point_selected
			added_point_informations = null
			get_map().get_node("PointRenderer").point = null
			get_map().get_node("PointRenderer").update()
			
			
	
func create_territory():
	new_polygon_point = polygon_point_packed.instance()
	new_polygon_point.position = get_map().get_global_mouse_position()
	connect_polygon_point(new_polygon_point)
	get_map().get_node("NewPolygonRenderer").start_point = new_polygon_point
	get_map().get_node("PolygonPointRenderer").start_point = new_polygon_point
	get_map().get_main_area().add_child(new_polygon_point)
	
func set_territory_is_see(is_see):
	territory_edited.is_see = is_see
	
func rename_territory(new_name):
	territory_edited.territory_name = new_name
	
func delete_territory():
	for slot in territory_edited.get_node("Slots").get_children():
		slot.remove_neighbours()
	territory_edited.queue_free()
	for pol in get_tree().get_nodes_in_group("PolygonPoint"):
		pol.queue_free()
	
	get_map().get_node("TerritoryZoneRenderer").start_point = null
	get_map().get_node("PolygonPointRenderer").start_point = null
	
	yield(get_tree(), "idle_frame")
	
	get_map().get_node("SlotsRenderer").update()
	
func delete_point():
	polygon_point_inspected.previous.next = polygon_point_inspected.next
	polygon_point_inspected.next.previous = polygon_point_inspected.previous
	if get_map().get_node("TerritoryZoneRenderer").start_point == polygon_point_inspected:
		get_map().get_node("TerritoryZoneRenderer").start_point = polygon_point_inspected.next
		get_map().get_node("PolygonPointRenderer").start_point = polygon_point_inspected.next
	get_map().get_node("TerritoryZoneRenderer").update()
	get_map().get_node("PolygonPointRenderer").update()
	polygon_point_inspected.queue_free()
	var polygon = get_map().get_node("TerritoryZoneRenderer").start_point.get_polygon()
	territory_edited.get_node("CollisionPolygon2D").polygon = polygon
	
func _enable():
	get_map().get_node("TerritoryZoneRenderer").visible = true
	get_map().get_node("PolygonPointRenderer").visible = true
	get_map().get_node("TerritoryEnteredDraw").visible = true
	for territory in get_tree().get_nodes_in_group("Territory"):
		connect_territory(territory)
		
func _disable():
	get_map().get_node("TerritoryZoneRenderer").visible = false
	get_map().get_node("PolygonPointRenderer").visible = false
	get_map().get_node("TerritoryEnteredDraw").visible = false
	for territory in get_tree().get_nodes_in_group("Territory"):
		disconnect_territory(territory)
	
func _save(data):
	data.territories = {} if not data.has("territories") else data.territories
	
	for territory in get_tree().get_nodes_in_group("Territory"):
		var territory_data = {} if not data.territories.has(territory.get_instance_id()) else data.territories[territory.get_instance_id()]
		
		territory_data.name = territory.name
		territory_data.territory_name = territory.territory_name
		territory_data.is_see = territory.is_see
		
		territory_data.polygon = []
		
		for point in territory.get_node("CollisionPolygon2D").polygon:
			territory_data.polygon.push_back([point.x, point.y])
		
		data.territories[territory.get_instance_id()] = territory_data
		
	return data

func _import(data):
	data._territories_by_id = {} if not data.has("_territories_by_id") else data._territories_by_id
	
	if data.territories: 
		for territory_id in data.territories:
			var territory_data = data.territories[territory_id]
			var territory = territory_packed.instance()
			territory.is_see = territory_data.is_see
			territory.territory_name = territory_data.territory_name
			var polygon = []
			for p in territory_data.polygon:
				polygon.push_back(Vector2(p[0], p[1]))
			territory.get_node("CollisionPolygon2D").polygon = polygon
			get_map().get_main_area().add_child(territory)
			if is_processing() : connect_territory(territory)
			data._territories_by_id[territory_id] = territory

func _on_territory_entered(territory):
	get_map().highlight_territory(territory, "Entered")
	if territory == territory_edited or polygon_point_selected or new_polygon_point:
		get_map().get_node("TerritoryEnteredDraw").visible = false
	
func _on_territory_exited(territory):
	get_map().unhighlight_territory(territory, "Entered")
	if territory == territory_edited and not polygon_point_selected and not new_polygon_point:
		get_map().get_node("TerritoryEnteredDraw").visible = true
	
func _on_territory_clicked(territory):
	if added_point_informations or polygon_point_selected or not polygon_entered_stack.empty() or get_map().get_territory_by_state("Entered") != territory:
		return
	select_territory(territory)
	
func select_territory(territory):
	get_map().get_node("TerritoryEnteredDraw").visible = false
	
	for pol in get_tree().get_nodes_in_group("PolygonPoint"):
		pol.queue_free()
	
	yield(get_tree(), "idle_frame")
	
	territory_edited = territory
	
	var points = []
	
	for point in territory.get_node("CollisionPolygon2D").polygon:
		var polygon_point = polygon_point_packed.instance()
		connect_polygon_point(polygon_point)
		polygon_point.position = point
		points.push_back(polygon_point)
		get_map().get_main_area().add_child(polygon_point)
		
	for i in range(points.size()) :
		var p = points[i]
		var nextp = points[(i + 1) % points.size()]
		nextp.previous = p
		p.next = nextp
	
	set_polygon_starting_point(points[0])
	
	yield(get_tree(), "idle_frame")
	
	refresh_polygon_points()
	
	get_map().emit_signal("territory_inspected", territory)
	
func _on_polygon_point_selected(polygon_point):
	if not new_polygon_point:
		polygon_point_selected = polygon_point
	
func _on_polygon_point_unselected(polygon_point):
	if polygon_point_selected:
		var polygon = polygon_point_selected.get_polygon()
		territory_edited.get_node("CollisionPolygon2D").polygon = polygon
		polygon_point_selected = null
		get_map().get_node("TerritoryEnteredDraw").visible = true

func set_polygon_starting_point(starting_point):
	get_map().get_node("TerritoryZoneRenderer").start_point = starting_point
	get_map().get_node("PolygonPointRenderer").start_point = starting_point
	
func refresh_polygon_points():
	get_map().get_node("TerritoryZoneRenderer").update()
	get_map().get_node("PolygonPointRenderer").update()
	get_map().get_node("NewPolygonRenderer").update()

func _on_polygon_point_transform_changed(polygon_point):
	refresh_polygon_points()

func _on_polygon_point_entered(polygon_point):
	polygon_entered_stack.push_back(polygon_point)
	get_map().get_node("TerritoryEnteredDraw").visible = false
	
func _on_polygon_point_exited(polygon_point):
	polygon_entered_stack.remove(polygon_entered_stack.find(polygon_point))
	if polygon_entered_stack.empty() and get_map().get_territory_by_state("Entered") != territory_edited:
		get_map().get_node("TerritoryEnteredDraw").visible = true
	
func _unhandled_input(event):
	if event is InputEventMouseMotion :
		var mouse_pos = get_map().get_global_mouse_position()
		if polygon_point_selected :
			polygon_point_selected.position = mouse_pos
		elif new_polygon_point:
			new_polygon_point.position = mouse_pos
		elif not polygon_entered_stack.empty():
			get_map().get_node("PointRenderer").point = null
			added_point_informations = null
		elif is_instance_valid(territory_edited):
			var territory_polygon = get_map().get_node("PolygonPointRenderer").start_point.get_polygon(true)
			var minimum_distance_edge_map = null
			for i in range(territory_polygon.size()):
				var v1: Vector2 = territory_polygon[i].position
				var v2: Vector2 = territory_polygon[(i + 1) % territory_polygon.size()].position
				var v: = v2 - v1
				var mv: Vector2 = mouse_pos - v1
				var dot = mv.dot(v)
				if dot > 0 :
					var p: = mv.project(v)
					if p.length_squared() < v.length_squared() :
						var distance = mv.distance_to(p)
						if  ! minimum_distance_edge_map or minimum_distance_edge_map.minimum > distance:
							minimum_distance_edge_map = {
								"minimum" : mv.distance_to(p),
								"position" : v1 + p,
								"previous": territory_polygon[i],
								"next" : territory_polygon[(i + 1) % territory_polygon.size()]
							}
			
			if minimum_distance_edge_map and minimum_distance_edge_map.minimum < 3.0:
				added_point_informations = minimum_distance_edge_map
				get_map().get_node("PointRenderer").point = added_point_informations.position
			else:
				get_map().get_node("PointRenderer").point = null
				added_point_informations = null
				
	if event.is_action_pressed("touched"):
		if polygon_point_inspected:
			print("uninspected")
			polygon_point_inspected = null
			get_map().emit_signal("polygon_point_uninspected", polygon_point_inspected)
				
func _on_polygon_point_inspected(polygon_point):
	get_map().emit_signal("polygon_point_inspected", polygon_point)
	polygon_point_inspected = polygon_point

func connect_polygon_point(polygon_point):
	polygon_point.connect("selected", self, "_on_polygon_point_selected")
	polygon_point.connect("unselected", self, "_on_polygon_point_unselected")
	polygon_point.connect("entered", self, "_on_polygon_point_entered")
	polygon_point.connect("exited", self, "_on_polygon_point_exited")
	polygon_point.connect("inspected", self, "_on_polygon_point_inspected")
	polygon_point.connect("transform_changed", self, "_on_polygon_point_transform_changed")
	
func disconnect_polygon_point(polygon_point):
	polygon_point.disconnect("selected", self, "_on_polygon_point_selected")
	polygon_point.disconnect("unselected", self, "_on_polygon_point_unselected")
	polygon_point.disconnect("entered", self, "_on_polygon_point_entered")
	polygon_point.disconnect("exited", self, "_on_polygon_point_exited")
	polygon_point.disconnect("inspected", self, "_on_polygon_point_inspected")
	polygon_point.disconnect("transform_changed", self, "_on_polygon_point_transform_changed")
	
func connect_territory(territory):
	territory.connect("entered", self, "_on_territory_entered")
	territory.connect("exited", self, "_on_territory_exited")
	territory.connect("clicked", self, "_on_territory_clicked")
	
func disconnect_territory(territory):
	territory.disconnect("entered", self, "_on_territory_entered")
	territory.disconnect("exited", self, "_on_territory_exited")
	territory.disconnect("clicked", self, "_on_territory_clicked")
