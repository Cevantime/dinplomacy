extends Area2D

class_name PolygonPoint

var previous
var previous_pos = Vector2()
var next
var index

signal selected(polygon_point)
signal unselected(polygon_point)
signal inspected(polygon_point)
signal entered(polygon_point)
signal exited(polygon_point)
signal transform_changed(polygon_point)

func _process(delta):
	if position != previous_pos :
		emit_signal("transform_changed", self)
		previous_pos = position
		
func get_polygon(asPolygonPoints = false):
	var point := self
	var points = [point if asPolygonPoints else point.position]
	while true:
		var next_point = point.next
		if not next_point or next_point == self:
			break
		points.push_back(next_point if asPolygonPoints else next_point.position)
		point = next_point
	return points

func _on_PolygonPoint_input_event(viewport, event, shape_idx):
	if event.is_action_pressed("touched"):
		emit_signal("selected", self)
	elif event.is_action_released("touched"):
		emit_signal("unselected", self)
	elif event.is_action_pressed("inspected"):
		emit_signal("inspected", self)

func _on_PolygonPoint_mouse_entered():
	emit_signal("entered", self)

func _on_PolygonPoint_mouse_exited():
	emit_signal("exited", self)
