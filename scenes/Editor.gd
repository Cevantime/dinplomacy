extends Node


var neighbour_element_packed: PackedScene = preload("res://scenes/ui/NeighbourElement.tscn")
var slot_packed: PackedScene = preload("res://scenes/Slot.tscn")
var editor_map_packed = preload("res://scenes/maps/EditorMap.tscn")

func _ready():
	var default_map_path = PlayerVariables.get_data(PlayerVariables.SETTINGS_EDITOR_DEFAULT_MAP)
	if default_map_path:
		load_from_path(default_map_path)
		get_save_button().disabled = false
	else :
		get_save_button().disabled = true

func get_save_button():
	return $MarginContainer/HBoxContainer/MarginContainer/Panel/VBoxContainer/Actions/SaveButton

func get_map() -> EditorMap:
	return $MarginContainer/HBoxContainer/ViewportContainer/Viewport/EditorMap as EditorMap
func get_slot_inspector():
	return $MarginContainer/HBoxContainer/MarginContainer/Panel/VBoxContainer/TabContainer/Slots/Inspector/VBoxContainer
func get_territory_inspector():
	return $MarginContainer/HBoxContainer/MarginContainer/Panel/VBoxContainer/TabContainer/Territories/Inspector
func get_neighbours_list():
	return $MarginContainer/HBoxContainer/MarginContainer/Panel/VBoxContainer/TabContainer/Slots/Inspector/VBoxContainer/ScrollContainer/NeighboursList
func get_slot_name_edit():
	return $MarginContainer/HBoxContainer/MarginContainer/Panel/VBoxContainer/TabContainer/Slots/Inspector/VBoxContainer/HBoxContainer/NameEdit
	
func _on_SaveAsButton_pressed():
	$FileSaveAsDialog.popup()

func _on_EditorMap_slot_inspected(slot):
	for child in get_neighbours_list().get_children():
		child.queue_free()
	
	$MarginContainer/HBoxContainer/MarginContainer/Panel/VBoxContainer/TabContainer/Slots/Inspector/VBoxContainer/IsFleetCheckBox.pressed = slot.is_fleet
	get_slot_name_edit().text = slot.slot_name
	
	for neighbour in slot.neighbours:
		var neighbour_element = neighbour_element_packed.instance()
		neighbour_element.slot = neighbour
		neighbour_element.connect("delete_request", self, "_on_NeighbourElement_delete_request")
		get_neighbours_list().add_child(neighbour_element)

	get_slot_inspector().show()

func uninspect():
	get_slot_inspector().hide()
	for child in get_neighbours_list().get_children():
		child.queue_free()
		
func _on_EditorMap_slot_uninspected(slot):
	uninspect()

func _on_NeighbourElement_delete_request(element, slot):
	get_map().remove_neighbour(slot)
	element.queue_free()

func _on_CheckBox_toggled(button_pressed):
	get_map().set_is_fleet(button_pressed)

func _on_ImportButton_pressed():
	$FileImportDialog.popup()

func load_from_path(path):
	var input = File.new()
	input.open(path, File.READ)
	var json = JSON.parse(input.get_as_text())
	if json.error:
		print("something wrong appended")
		return
	get_map().import(json.result)

func _on_FileImportDialog_file_selected(path):
	load_from_path(path)
	PlayerVariables.set_data(PlayerVariables.SETTINGS_EDITOR_DEFAULT_MAP, path)
	PlayerVariables.save_data()
	get_save_button().disabled = false
	
func save_in_path(path):
	var data = get_map().save()
		
	var output = File.new()
	
	output.open(path, File.WRITE)
	output.store_string(JSON.print(data))
	output.close()
			
func _on_DeleteButton_pressed():
	get_map().delete_slot()
	self.uninspect()

func _on_RenameButton_pressed():
	get_map().rename_slot(get_slot_name_edit().text)

func _on_FileSaveAsDialog_file_selected(path):
	save_in_path(path)
	PlayerVariables.set_data(PlayerVariables.SETTINGS_EDITOR_DEFAULT_MAP, path)
	PlayerVariables.save_data()
	get_save_button().disabled = false

func _on_SaveButton_pressed():
	save_in_path(PlayerVariables.get_data(PlayerVariables.SETTINGS_EDITOR_DEFAULT_MAP))

func _on_AddNewSlotButton_pressed():
	get_map().create_slot()

func _on_EditorMap_slot_drag_start(slot):
	$MarginContainer/HBoxContainer/MarginContainer/Panel/VBoxContainer.visible = false

func _on_EditorMap_slot_drag_end(slot):
	$MarginContainer/HBoxContainer/MarginContainer/Panel/VBoxContainer.visible = true

func _on_TabContainer_tab_changed(tab):
	match tab:
		1 : get_map().change_to("Territories")
		2 : get_map().change_to("Slots")

func _on_AddTerritoryButton_pressed():
	get_map().create_territory()


func _on_EditorMap_territory_inspected(territory):
	$MarginContainer/HBoxContainer/MarginContainer/Panel/VBoxContainer/TabContainer/Territories/Inspector/VBoxContainer/TerritoryInfos/HBoxContainer2/IsSeeCheckBox.pressed = territory.is_see
	$MarginContainer/HBoxContainer/MarginContainer/Panel/VBoxContainer/TabContainer/Territories/Inspector/VBoxContainer/TerritoryInfos/HBoxContainer/TerritoryNameEdit.text = territory.territory_name
	get_territory_inspector().show()


func _on_IsSeeCheckBox_toggled(button_pressed):
	get_map().set_territory_is_see(button_pressed)

func _on_RenameTerritoryButton_pressed():
	get_map().rename_territory($MarginContainer/HBoxContainer/MarginContainer/Panel/VBoxContainer/TabContainer/Territories/Inspector/VBoxContainer/TerritoryInfos/HBoxContainer/TerritoryNameEdit.text)


func _on_DeleteTerritoryButton_pressed():
	get_map().delete_territory()
	get_territory_inspector().hide()

func _on_EditorMap_polygon_point_inspected(polygon_point):
	$MarginContainer/HBoxContainer/MarginContainer/Panel/VBoxContainer/TabContainer/Territories/Inspector/VBoxContainer/PointInfos.show()

func _on_EditorMap_polygon_point_uninspected(polygon_point):
	$MarginContainer/HBoxContainer/MarginContainer/Panel/VBoxContainer/TabContainer/Territories/Inspector/VBoxContainer/PointInfos.hide()


func _on_DeletePointButton_pressed():
	get_map().delete_point()
	$MarginContainer/HBoxContainer/MarginContainer/Panel/VBoxContainer/TabContainer/Territories/Inspector/VBoxContainer/PointInfos.hide()

func _on_LoadImageButton_pressed():
	$ImageMapDialog.popup()
	

func create_new_map():
	$MarginContainer/HBoxContainer/MarginContainer/Panel/VBoxContainer/TabContainer.tabs_visible = false
	# PlayerVariables.set_data(PlayerVariables.SETTINGS_EDITOR_DEFAULT_MAP, null)
	var viewport = $MarginContainer/HBoxContainer/ViewportContainer/Viewport
	viewport.remove_child(get_map())
	var map = editor_map_packed.instance()
	viewport.add_child(map)
	map.name = "EditorMap"
	map.owner = viewport
	

func _on_NewMapButton_pressed():
	create_new_map()


func _on_ImageMapDialog_file_selected(path):
	var err = get_map().set_image(path)
	if !err:
		$MarginContainer/HBoxContainer/MarginContainer/Panel/VBoxContainer/TabContainer.tabs_visible = true
		$MarginContainer/HBoxContainer/MarginContainer/Panel/VBoxContainer/TabContainer.current_tab = 1
