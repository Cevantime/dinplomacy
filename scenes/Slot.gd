tool
extends Area2D

class_name Slot

export(Array, NodePath) var neighbours: Array
export(bool) var is_fleet setget set_is_fleet
export(String) var slot_name

var previous_pos = Vector2()
var dragged = false

signal clicked(slot)
signal entered(slot)
signal exited(slot)
signal inspected(slot)
signal is_fleet_changed(slot, is_fleet)
signal transform_changed(slot)
signal drag_start(slot)

func _ready():
	if ! slot_name :
		slot_name = name
		
func _process(delta):
	if position != previous_pos :
		emit_signal("transform_changed", self)
		previous_pos = position
	
	
func add_child(child: Node, create_eligible_name: bool = false):
	assert(child is Troup and child.is_fleet == is_fleet)
	if get_child_count() == 3 :
		remove_child(get_child(2))
	.add_child(child, create_eligible_name)
	
func get_nation():
	var territory = get_territory()
	if territory:
		return territory.get_nation()
	return Nations.NAMES.NEUTRAL

func get_territory():
	var path_territory = "../.."
	if is_instance_valid(get_node(path_territory)):
		print(get_node(path_territory).name)
		return get_node(path_territory)
	return null
	
func add_neighbour(neighbour: Slot):
	if neighbours.find(neighbour) == -1:
		neighbours.push_back(neighbour)
		neighbour.neighbours.push_back(self)
	
func remove_neighbour(neighbour: Slot):
	var index = neighbours.find(neighbour)
	if index >= 0 :
		neighbours.remove(index)
		neighbour.remove_neighbour(self)
		
func remove_neighbours():
	while self.neighbours:
		remove_neighbour(neighbours[0])
	
func set_is_fleet(_is_fleet):
	is_fleet = _is_fleet
	emit_signal("is_fleet_changed", self, is_fleet)
	
func _on_Slot_mouse_entered():
	emit_signal("entered", self)

func _on_Slot_mouse_exited():
	emit_signal("exited", self)
	
func _on_Slot_input_event(viewport, event, shape_idx):
	if event.is_action_pressed("touched"):
		print("timer started")
		$DragTimer.start()
		
	elif event.is_action_released("touched"):
		if not dragged :
			print("clicked")
			$DragTimer.stop()
			emit_signal("clicked", self)
			
	elif event.is_pressed() and event.is_action("inspected"):
		emit_signal("inspected", self)

func _on_DragTimer_timeout():
	print("drag_start")
	dragged = true
	emit_signal("drag_start", self)
	
