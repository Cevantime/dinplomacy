extends "res://scenes/Map.gd"

class_name GameplayMap

signal troup_selected(troup)
signal troup_deselected(troup)
signal troup_supported(troup)
signal troup_desupported(troup)
signal troup_convoyed(troup)
signal troup_deconvoyed(troup)
signal order_sent(order)

enum STATES {
	IDLE,
	HOLD,
	MOVE,
	SUPPORT_FROM,
	SUPPORT_TO,
	CONVOY_FROM,
	CONVOY_TO
}

var order
var state = STATES.IDLE

func set_state(_state):
	self.state = _state
	if _state == STATES.HOLD:
		var order = Order.new()
		order.from = troups.selected.get_node("../../..").get_name()
		order.action = Order.NAMES.HOLD
		emit_signal("order_sent", order)
		self.state = STATES.IDLE
		deselect_troup()
			
func deselect_troup() :
	emit_signal("troup_deselected", troups.selected)
	troups.selected = null
	unhighlight_troup(troups.selected, "Idle")

func select_troup(troup):
	troups.selected = troup
	highlight_troup(troup, "Idle")
	emit_signal("troup_selected", troup)

func select_troup_supported(troup):
	troups.supported = troup
	highlight_troup(troup, "Supported")
	emit_signal("troup_supported", troup)
	state = STATES.SUPPORT_TO

func unselect_troup_supported():
	emit_signal("troup_desupported", troups.supported)
	unhighlight_troup(troups.supported, "Supported")
	troups.supported = null

func select_troup_convoyed(troup):
	troups.convoyed = troup
	highlight_troup(troup, "Convoyed")
	emit_signal("troup_convoyed", troup)
	state = STATES.CONVOY_TO
	
func unselect_troup_convoyed():
	emit_signal("troup_convoyed", troups.convoyed)
	unhighlight_troup(troups.convoyed, "Convoyed")
	troups.convoyed = null
	
		
func _ready():
	for territory in get_tree().get_nodes_in_group("Territory"):
		territory.connect("entered", self, "_territory_entered")
		territory.connect("exited", self, "_territory_exited")
		territory.connect("clicked", self, "_territory_clicked")
		
	for troup in get_tree().get_nodes_in_group("Troup"):
		troup.connect("entered", self, "_troup_entered")
		troup.connect("exited", self, "_troup_exited")
		troup.connect("clicked", self, "_troup_clicked")

		
func _territory_entered(territory: Territory):
	if state == STATES.IDLE:
		highlight_territory(territory, "Idle")
	elif state == STATES.MOVE:
		highlight_territory(territory, "Moved")
	elif state == STATES.SUPPORT_TO:
		highlight_territory(territory, "Supported")
	elif state == STATES.CONVOY_TO:
		highlight_territory(territory, "Convoyed")
	
func _territory_exited(territory: Territory):
	if state == STATES.IDLE:
		unhighlight_territory(territory, "Idle")
	elif state == STATES.MOVE:
		unhighlight_territory(territory, "Moved")
	elif state == STATES.SUPPORT_TO:
		unhighlight_territory(territory, "Supported")
	elif state == STATES.CONVOY_TO:
		unhighlight_territory(territory, "Convoyed")
	
func _territory_clicked(territory: Territory):
	if state in [STATES.SUPPORT_FROM, STATES.IDLE, STATES.CONVOY_FROM]:
		return
		
	var order = Order.new()
	order.from = troups.selected.get_node("../../..").get_name()
	order.to = territory.get_name()
	
	if state == STATES.MOVE:
		order.action = Order.NAMES.MOVE
		unhighlight_territory(territory, "Moved")
		
	elif state == STATES.SUPPORT_TO:
		order.action = Order.NAMES.SUPPORT
		order.what = troups.supported.get_node("../../..").get_name()
		unselect_troup_supported()
		unhighlight_territory(territory, "Supported")
		
	elif state == STATES.CONVOY_TO:
		order.action = Order.NAMES.CONVOY
		order.what = troups.convoyed.get_node("../../..").get_name()
		unselect_troup_convoyed()
		unhighlight_territory(territory, "Convoyed")
	
	deselect_troup()
	call_deferred("set_state", STATES.IDLE)
	emit_signal("order_sent", order)
	
func _troup_entered(troup: Troup):
	if state == STATES.IDLE:
		if troups.selected != null:
			return
		highlight_troup(troup, "Idle")
		
	elif state == STATES.SUPPORT_FROM:
		highlight_troup(troup, "Supported")
		
	elif state == STATES.CONVOY_FROM:
		highlight_troup(troup, "Convoyed")
	
func _troup_exited(troup: Troup):
	if state == STATES.IDLE:
		if troup != troups.selected:
			unhighlight_troup(troup, "Idle")
			if troups.selected : highlight_troup(troups.selected, "Idle")
			
	elif state == STATES.SUPPORT_FROM:
		unhighlight_troup(troup, "Supported")
	elif state == STATES.CONVOY_FROM:
		unhighlight_troup(troup, "Convoyed")
	
func _troup_clicked(troup: Troup):
	if state == STATES.IDLE:
		select_troup(troup)
	elif state == STATES.SUPPORT_FROM:
		select_troup_supported(troup)
	elif state == STATES.CONVOY_FROM:
		select_troup_convoyed(troup)

