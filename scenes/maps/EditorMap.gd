extends "res://scenes/Map.gd"

class_name EditorMap

signal slot_inspected(slot)
signal slot_uninspected(slot)
signal slot_drag_start(slot)
signal slot_drag_end(slot)

signal polygon_point_inspected(polygon_point)
signal polygon_point_uninspected(polygon_point)

signal territory_inspected(territory)

func _ready():
	change_to("Territories")
	
func get_main_area():
	return $MainArea/ImageMap

func remove_neighbour(slot):
	$Processors/SlotsProcessor.slot_inspected.remove_neighbour(slot)

func set_is_fleet(is_fleet):
	$Processors/SlotsProcessor.slot_inspected.is_fleet = is_fleet
	
func delete_slot():
	$Processors/SlotsProcessor.slot_inspected.remove_neighbours()
	$Processors/SlotsProcessor.slot_inspected.queue_free()
	yield(get_tree(), "idle_frame")
	$SlotsRenderer.update()
	
func rename_slot(new_name):
	$Processors/SlotsProcessor.slot_inspected.slot_name = new_name
	
func create_slot():
	$Processors/SlotsProcessor.create_slot()
	
func create_territory():
	$Processors/TerritoriesProcessor.create_territory()
	
func set_territory_is_see(is_see):
	$Processors/TerritoriesProcessor.set_territory_is_see(is_see)
	
func rename_territory(new_name):
	$Processors/TerritoriesProcessor.rename_territory(new_name)
	
func delete_territory():
	$Processors/TerritoriesProcessor.delete_territory()
	
func delete_point():
	$Processors/TerritoriesProcessor.delete_point()
	
func change_to(name):
	set_current_processor(get_node("Processors/%sProcessor" % [name]))

