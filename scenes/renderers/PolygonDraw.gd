extends Node2D

export(Color) var color = Color(0,0,0)

var polygon setget set_polygon

func _draw():
	if ! polygon:
		return
	var close_point: Vector2 = polygon[polygon.size() - 1]
	close_point = Vector2(close_point.x, close_point.y)
	
	var points = [close_point]
	
	for p in polygon:
		points.append(p)
		
	draw_polyline(points, color, 20)
	
func set_polygon(polygon_to_set): 
	polygon = polygon_to_set
	update()
	
