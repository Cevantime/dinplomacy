tool
extends Node2D

export(Color) var army_color setget set_army_color
export(Color) var fleet_color setget set_fleet_color

var opacity = 1.0

var colors: Dictionary = {}

func _draw():
	for slot in get_tree().get_nodes_in_group("Slot"):
		if not colors.has(slot.get_instance_id()):
			reset_slot_color(slot)
		var c = colors[slot.get_instance_id()]
		draw_circle(slot.global_position, slot.get_node("CollisionShape2D").shape.radius, Color(c.r, c.g, c.b, opacity))
	
func change_slot_color(slot, color):
	colors[slot.get_instance_id()] = color;
	update()
	
func reset_slot_color(slot, update = true):
	colors[slot.get_instance_id()] = fleet_color if slot.is_fleet else army_color
	if update : update()

func _on_is_fleet_changed(slot, is_fleet):
	var current_color = colors[slot.get_instance_id()]
	var forbidden_color = army_color if is_fleet else fleet_color
	if current_color == forbidden_color :
		reset_slot_color(slot)
	
func _on_transform_changed(slot):
	update()
	
func set_army_color(color):
	army_color = color
	update()

func set_fleet_color(color):
	fleet_color = color
	update()
