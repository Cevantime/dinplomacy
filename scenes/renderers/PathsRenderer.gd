extends Node2D

var slot setget set_slot
var current_target setget set_current_target

func _draw():
	if ! is_instance_valid(slot):
		return
	for neighbour in slot.neighbours :
		draw_line(slot.global_position, neighbour.global_position, Color.rebeccapurple, 3.0, true)
	if current_target:
		draw_line(slot.global_position, current_target, Color.red, 3.0, true)

func refresh(slot):
	update()
	
func set_slot(_slot):
	slot = _slot
	update()

func set_current_target(_current_target):
	current_target = _current_target
	update()
