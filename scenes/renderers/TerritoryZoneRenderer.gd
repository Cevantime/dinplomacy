extends Node2D

var start_point: PolygonPoint setget set_start_point

export(Color) var fill_color: Color

func _draw():
	if not start_point:
		return
		
	var points = start_point.get_polygon()
		
	if points.size() == 1:
		return
	if points.size() == 2 :
		draw_line(points[0], points[1], fill_color)
	else:
		draw_polygon(points,[fill_color])

func set_start_point(_start_point):
	start_point = _start_point
	update()
