tool
extends Node2D

func _ready():
	for territory in get_tree().get_nodes_in_group("Territory"):
		territory.connect("nation_changed", self, "_change_territory_nation")

func _draw():
	for territory in get_tree().get_nodes_in_group("Territory"):
		if territory.get_nation() and territory.get_nation() != Nations.NAMES.NEUTRAL :
			var c = Nations.get_color(territory.get_nation())
			draw_polygon(territory.get_node("CollisionPolygon2D").polygon, [Color(c.r, c.g, c.b, 0.6)])
	
func _change_territory_nation(territory, nation):
	update()
