extends Node2D

var start_point
export(Color) var color

func _draw():
	if ! start_point :
		return
	var polygon = start_point.get_polygon()
	if polygon.size() >= 2:
		draw_polyline(start_point.get_polygon(), color, 3.0, true)
	
