extends Node2D

var start_point: PolygonPoint setget set_start_point

export(Color) var edge_color: Color

func _draw():
	if not start_point:
		return
	var points = start_point.get_polygon(true)
	
	for point in points:
		draw_circle(point.position, point.get_node("CollisionShape2D").shape.radius,edge_color)

func set_start_point(_start_point):
	start_point = _start_point
	update()
