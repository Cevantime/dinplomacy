extends Node2D

var point setget set_point
export(Color) var color

func _draw():
	if point :
		draw_circle(point, 5.0, color)

func set_point(_point):
	point = _point
	update()
