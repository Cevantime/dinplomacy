tool
extends Area2D

class_name Territory

signal entered(territory)
signal exited(territory)
signal clicked(territory)
signal nation_changed(territory, nation)

export(Nations.NAMES) var nation = Nations.NAMES.NEUTRAL setget set_nation, get_nation
export(bool) var is_see = false

var troup
var territory_name

func _ready():
	if ! territory_name:
		territory_name = name

func get_color():
	return $"/root/NationsAuthority".get_color(nation)
	
func add_slot(slot):
	$Slots.add_child(slot)
	slot.set_owner($Slots)
	
func remove_slot(slot):
	$Slots.remove_child(slot)
	
func get_nation():
	return nation

func set_nation(nation_to_set):
	nation = nation_to_set
	emit_signal("nation_changed", self, nation_to_set)
	
func _on_Territory_mouse_entered():
	emit_signal("entered", self)

func _on_Territory_mouse_exited():
	emit_signal("exited", self)

func _on_Territory_input_event(viewport, event, shape_idx):
	if event.is_pressed() and event.is_action("touched"):
		emit_signal("clicked", self)
