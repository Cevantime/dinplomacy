extends Node2D

class_name Map

var territory_stacks: Dictionary = {}
var troups: Dictionary = {}
var current_processor: Node

const ref_dim = 3000

func _ready():
	for processor in $Processors.get_children():
		processor.map = self
		processor._start()
		set_processed(processor, false)
	
func set_image(imagePath):
	var image = Image.new()
	var err = image.load(imagePath)
	
	if err :
		return err
	
	var texture = ImageTexture.new()
	texture.create_from_image(image, Texture.FLAGS_DEFAULT)
	$MainArea/ImageMap.texture = texture
	adapt_background()
		
func adapt_background():
	var image_size = $MainArea/ImageMap.get_rect().size
	var background_scale = Vector2(1,1)
	background_scale.x = image_size.x / ref_dim
	background_scale.y = image_size.y / ref_dim
	var background_size = $BackgroundColor.rect_size * background_scale
	$BackgroundColor.rect_position.x = - (background_size.x - image_size.x) / 2
	$BackgroundColor.rect_position.y = - (background_size.y - image_size.y) / 2
	$BackgroundColor.rect_scale = background_scale
	$Camera2D.setup_zoom()
	
func import(data):
	if ! data:
		return
	
	for territory in get_tree().get_nodes_in_group("Territory"):
		territory.queue_free()
	
	yield(get_tree(), "idle_frame")
	
	for processor in $Processors.get_children():
		processor._import(data)
		
func save():
	var data = {}
	for processor in $Processors.get_children():
		data = processor._save(data)
	
	return data

func set_processed(processor, processed):
	processor.set_process(processed)
	processor.set_process_input(processed)
	processor.set_process_unhandled_input(processed)
	processor.set_process_unhandled_key_input(processed)
	processor.set_process_internal(processed)
	
func set_processor_state(processor, enabled):
	set_processed(processor, enabled)
	if enabled : processor._enable()
	else : processor._disable()

func set_current_processor(new_processor):
	if current_processor :
		set_processor_state(current_processor, false)
	current_processor = new_processor
	set_processor_state(new_processor, true)
	current_processor.map = self
	

func highlight_troup(troup, type):
	var renderer = get_node("Troup"+type+"Draw")
	renderer.visible = true
	renderer.position = troup.global_position
	renderer.polygon = troup.get_node("CollisionPolygon2D").polygon

func unhighlight_troup(troup, type):
	var renderer = get_node("Troup"+type+"Draw")
	renderer.visible = false
	
func get_territory_stack_from_type(type):
	if not territory_stacks.has(type) :
		territory_stacks[type] = []
		
	return territory_stacks[type]
	
func get_territory_by_state(state):
	if territory_stacks.has(state) and not territory_stacks[state].empty():
		return territory_stacks[state][territory_stacks[state].size() -1]
	
func highlight_territory(territory, type):
	var stack = get_territory_stack_from_type(type)
	stack.push_back(territory)
	var renderer = get_node("Territory"+type+"Draw")
	# renderer.visible = true
	renderer.polygon = territory.get_node("CollisionPolygon2D").polygon
	
func unhighlight_territory(territory, type):
	var stack = get_territory_stack_from_type(type)
	var territory_index = stack.find(territory)
	if territory_index >= 0: stack.remove(territory_index)
	
	var renderer = get_node("Territory"+type+"Draw")
	if stack.empty():
		renderer.polygon = null
		# renderer.visible = false
	else :
		renderer.polygon = stack[stack.size() - 1].get_node("CollisionPolygon2D").polygon
