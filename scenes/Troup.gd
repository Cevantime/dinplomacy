tool
extends Area2D

class_name Troup

signal entered(Troup)
signal exited(Troup)
signal clicked(Troup)

export(bool) var is_fleet = false
export(Nations.NAMES) var nation = Nations.NAMES.NEUTRAL setget set_nation

var territory

func _enter_tree():
	territory = $'../../..'
	territory.troup = self
	set_color()
	
func set_nation(nation_to_set): 
	nation = nation_to_set
	call_deferred("set_color")


func set_color():
	var color = $"/root/NationsAuthority".get_color(nation)
	$Sprite.modulate = Color(color.r + 0.2, color.g + 0.2, color.b + 0.2, 1) 

func display_actions():
	$Actions.visible = true
	
func hide_actions():
	$Actions.visible = false

func _on_Troup_mouse_entered():
	emit_signal("entered", self)

func _on_Troup_mouse_exited():
	emit_signal("exited", self)

func _on_Troup_input_event(viewport, event, shape_idx):
	if event.is_pressed() && event.is_action("touched"):
		emit_signal("clicked", self)
		
